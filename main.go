package main

import (
	"fmt"
	"tipe-data/library"
)

func main() {
	/*
		penulisan variable terbagi menjadi 2 yaitu menggunakan
		1. var <nama-variable> <tipe-data> = <value> / bisa dengan var <nama-variable> = <value>
		example = var inicontoh string = "ini type data string"
		2. <nama-variable> := <nilai> untuk lebih singkat bisa menggunakn tanda :=
		jika sudah menggunakan var tidak diperbolehkan menggunakan simbol tanda :=
	*/
	var riyad = 123
	riyada := "Ini string"
	fmt.Printf("%d\n", riyad)
	fmt.Printf("%s\n", riyada)

	/*
		tipe data terbagi menjadi 4 :
		1. numerik non-decimal
		2. numerik decimal
		3. boolean ( bool ) tipe data bool hanya bisa diisi oleh true or false
		4. string
		dapat dilihat lebih lengkap di https://dasarpemrogramangolang.novalagung.com/A-tipe-data.html
	*/

	fmt.Println("==== Materi tentang IF ELSE =====")
	nilai := 92

	if nilai >= 80 {
		fmt.Println("Kondisi ini true")
	} else {
		fmt.Println("Kondisi ini false")
	}

	fmt.Println("==== Materi tentang IF ELSE IF =====")
	if nilai <= 100 && nilai >= 91 {
		fmt.Println("Nilai anda A")
	} else if nilai <= 90 && nilai >= 71 {
		fmt.Println("Nilai anda B")
	} else if nilai <= 70 && nilai >= 51 {
		fmt.Println("Nilai anda C")
	} else if nilai <= 50 && nilai >= 0 {
		fmt.Println("Nilai anda D")
	} else {
		fmt.Println("Opps, sepertinya nilai andara lebih kecil dari 0 atau lebih besar dari 100")
	}

	fmt.Println("==== Materi tentang SWITCH CASE =====")
	jenkel := "P"

	switch jenkel {
	case "P":
		fmt.Println("Perempuan")
	case "L":
		fmt.Println("Laki-Laki")
	default:
		fmt.Println("Jenis kelamin mu apa????")
	}

	fmt.Println("==== Materi tentang Three Component Loop =====")
	for i := 0; i < 5; i++ {
		fmt.Println("Three component loop ", i)
	}

	fmt.Println("==== Materi tentang Loop - Array =====")
	nama := [...]string{"riyad", "azmi", "zila"}

	for i, namas := range nama {
		fmt.Printf("Nama %d : %s\n", i, namas)
	}

	fmt.Println("==== Materi tentang Loop - with - conditional =====")
	x := 0

	for x < 5 {
		x *= 3
		x++
	}
	fmt.Printf("%d\n", x)

	fmt.Println("==== Materi tentang Loop - with - conditional and exit loop =====")
	sum := 0
	for i := 0; i < 5; i++ {
		if i == 3 {
			break
		}
		sum += i
	}
	fmt.Printf("%d\n", sum)

	fmt.Println("==== Materi tentang Package =====")
	library.Library()
}
