package main

import "fmt"

type student struct {
	name   string
	jenkel string
	nohp   uint
	nim    uint
	email  string
}

type detailstudent struct {
	alamat   string
	namaortu string
	detail   student
}

func newStudent(alamat string, namaortu string, name string, nohp uint, nim uint, email string) *detailstudent {
	new := detailstudent{
		alamat:   alamat,
		namaortu: namaortu,
		detail: student{
			name:  name,
			nohp:  nohp,
			nim:   nim,
			email: email,
		},
	}
	return &new

}

func main() {

	fmt.Println(detailstudent{alamat: "jl.bisa", namaortu: "siapa", detail: student{name: "riyad", nohp: 812869375, nim: 1612500197, email: "riyadlusholihin@gmail.com"}})
	fmt.Println(newStudent("jl.h.rusin", "zainuri", "riyad", 81286937597, 1612500197, "riyadlusholihin@gmail.com"))
}
