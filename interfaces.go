package main

import (
	"fmt"
)

type persegi interface {
	luas() float64
	keliling() float64
}

type segi struct {
	sisi float64
}

func (s segi) luas() float64 {
	return s.sisi * s.sisi
}

func (s segi) keliling() float64 {
	return 4 * s.sisi
}

func main() {
	s := segi{sisi: 5}

	fmt.Println("Luas Persegi : ", s.luas())
	fmt.Println("Keliling Persegi: ", s.keliling())
}
